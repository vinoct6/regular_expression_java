package com.regex;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

public class RegexTest {

    @Test
    public void isValidPhoneNumber() {
        //Assume valid Phone numbers start with 998 and 865

        final String PATTERN = "^(998|865)";

        String phone1 = "998234567";
        String phone2 = "865233467";
        String phone3 = "987253998";

        assertThat(patternMatchesInput(phone1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(phone2, PATTERN)).isTrue();
        assertThat(patternMatchesInput(phone3, PATTERN)).isFalse();
    }

    @Test
    public void matchCharacterOneOrMoreTimes() {
        //Matches r multiple times
        final String PATTERN = "(ar+)";

        String input1 = "ar";
        String input2 = "arr";
        String input3 = "arrr";
        String input4 = "abc";

        assertThat(patternMatchesInput(input1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(input2, PATTERN)).isTrue();
        assertThat(patternMatchesInput(input3, PATTERN)).isTrue();
        assertThat(patternMatchesInput(input4, PATTERN)).isFalse();

    }

    @Test
    public void matchGroupOfCharactersOneOrMoreTimes() {
        //Matches r multiple times
        final String PATTERN = "(ar)+";

        String input1 = "ar";
        String input2 = "arr";
        String input3 = "arar";
        String input4 = "axr";

        assertThat(patternMatchesInput(input1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(input2, PATTERN)).isTrue();
        assertThat(patternMatchesInput(input3, PATTERN)).isTrue();
        assertThat(patternMatchesInput(input4, PATTERN)).isFalse();
    }

    @Test
    public void matchLowerCaseWord() {
        final String PATTERN = "[a-z]+";
        String name1 = "mark";
        String name2 = "TAYLOR";
        assertThat(patternMatchesInput(name1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(name2, PATTERN)).isFalse();
    }

    @Test
    public void matchWordCaseInsensitive() {
        final String PATTERN = "(?i)[a-z]+";

        String name1 = "mark";
        String name2 = "TAYLOR";
        assertThat(patternMatchesInput(name1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(name2, PATTERN)).isTrue();
    }


    @Test
    public void matchWordWithWhiteSpace() {
        //  \s is used to denote white space
        final String PATTERN = "[a-zA-Z]+ ";

        String name1 = "Mark Taylor";
        String name2 = "MarkTaylor";

        assertThat(patternMatchesInput(name1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(name2, PATTERN)).isFalse();
    }

    @Test
    public void validateEmail() {
        String email = "vinoct6@gmail.com";
        /*
            1. Ignore case
            2. Start with sequence of chars
            3. @ symbol
            4. Another sequence of chars
            5. Match a literal .
            6. Match com, net or gov
            7. No chars after that
         */

        final String PATTERN1 = "(?)^(\\w+@\\w+\\.(com|net|gov)$)";
        assertThat(patternMatchesInput(email, PATTERN1)).isTrue();
    }

    @Test
    public void validateWithBoundaryMetaChars() {
        //   '\b' is the boundary meta character
//        String s = "These Violent Delights Have Violent Ends";

        String s1 = "ok";
        String s2 = "okay";

        final String PATTERN = "(\\bok\\b)";
        assertThat(patternMatchesInput(s1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(s2, PATTERN)).isFalse();
    }


    @Test
    public void validateMakingCharsOptional() {
        String s1 = "ok";
        String s2 = "okay";
        final String PATTERN = "(\\bok(ay)?\\b)";
        assertThat(patternMatchesInput(s1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(s2, PATTERN)).isTrue();
    }


    @Test
    public void validateNegateNumbers(){
        String s1 = "8475";
        final String PATTERN = "(?!)(\\d+)";
        assertThat(patternMatchesInput(s1, PATTERN)).isFalse();
    }


    @Test
    public void doNotMatchTwoDots(){
        String s1 = "abc.de";
        String s2 ="abc..de";
        String s3 ="abc.d.e";
        String s4 ="abc.d.e..f";

        final String PATTERN = "^\\w+(\\.(?!\\.))+\\w+$";
        assertThat(patternMatchesInput(s1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(s2, PATTERN)).isFalse();
        assertThat(patternMatchesInput(s3, PATTERN)).isTrue();
//        assertThat(patternMatchesInput(s4, PATTERN)).isFalse();
    }

    @Test
    public void matchHrefTag(){
        String link = "<a href=\"https://www.google.com\">Googfasdfs234 729le</a>";
        String link1="<a href=\"http://stackoverflow.com\" class=\"-logo js-gps-track _glyph\"\n" +
                "               data-gps-track=\"top_nav.click({is_current:false, location:2, destination:8})\">";

        String link2="<link rel=\"canonical\" href=\"http://wiprodigital.com/2017/03/23/bringing-digital-design-thinking-wipro-designits-story/\" />";
        String link3= "  <link rel=\"pingback\" href=\"http://wiprodigital.com/xmlrpc.php\">\n";


        final String PATTERN = "^<a href=\"(.)+\">(.)*</a>$";
        final String PATTERN1 = "href=\"(.)+\"";
        assertThat(patternMatchesInput(link, PATTERN)).isTrue();
        assertThat(patternMatchesInput(link1, PATTERN1)).isTrue();
        assertThat(patternMatchesInput(link2, PATTERN1)).isTrue();
        assertThat(patternMatchesInput(link3, PATTERN1)).isTrue();
    }

    @Test
    public void validateNegation(){
        //Validate if a string does not have any numbers
        // use ^\d or
        String s1 = "948Hello";
        String s2 = "Hello World";
        String s3 = "HelloWorld";

        final String PATTERN = "[(?!\\d)\\w+\\s]+";

        assertThat(patternMatchesInput(s1, PATTERN)).isTrue();
        assertThat(patternMatchesInput(s2, PATTERN)).isTrue();
        assertThat(patternMatchesInput(s3, PATTERN)).isTrue();
    }


   /*@Test
   public void validateURL(){
       String s1 = "http://google.com";
       String s2 = "https://en.wikipedia.org/wiki/Pale_Blue_Dot#/media/File:Pale_Blue_Dot.png";
       String s3 = "https://news.ycombinator.com/item?id=14217659";
       String s4 =" https://stackoverflow.com/questions/43681847/condition-vs-another-loop-performance-wise";
       String s5 = "https://www.cs.virginia.edu/~zaher/classes/CS656/levy.pdf";
       String s6 = "https://news.ycombinator.com";

       final String PATTERN_URL="^(http|https)://(\\w\\.)+\\.com";
       assertThat(patternMatchesInput(s1, PATTERN_URL)).isTrue();
//       assertThat(patternMatchesInput(s3, PATTERN_URL)).isTrue();
       assertThat(patternMatchesInput(s6, PATTERN_URL)).isTrue();


   }*/


    private Matcher getMatcher(final String stringToMatch, final String regexPattern) {
        Pattern pattern = Pattern.compile(regexPattern);
        return pattern.matcher(stringToMatch);
    }

    private boolean patternMatchesInput(final String stringToMatch, final String regexPattern) {
        Matcher matcher = getMatcher(stringToMatch, regexPattern);
        return matcher.find();
    }
}
